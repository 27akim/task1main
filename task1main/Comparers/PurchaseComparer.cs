﻿using System.Collections.Generic;
using task1main.Modules;

namespace task1main.Comparers
{
    class PurchaseComparer : IComparer<Purchase>
    {
        public int Compare(Purchase x, Purchase y)
        {
            if (x.Name.CompareTo(y.Name) > 0)
                return 1;
            else if (x.Name.CompareTo(y.Name) < 0)
                return -1;
            else if (x.Name.CompareTo(y.Name) == 0)
            {
                if (x.GetType() == typeof(Purchase) && y.GetType() == typeof(PricePurchase))
                    return -1;
                else if (x.GetType() == typeof(PricePurchase) && y.GetType() == typeof(Purchase))
                    return 1;
                else
                {
                    if (x.GetCost().CompareTo(x.GetCost()) > 0)
                        return 1;
                    else if (x.GetCost().CompareTo(x.GetCost()) < 0)
                        return -1;
                    else
                        return 0;
                }
            }
            else
                return 0;
        }
    }
}

