﻿using System;

namespace task1main.Exeptions
{
    class CsvLineException : ApplicationException

    {
        public string Line { get; }
        public CsvLineException(string csvString, string message) : base(message)
        {
            Line = csvString;
        }
    }
}