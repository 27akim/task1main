﻿using System;

namespace task1main.Modules
{
    class PricePurchase : Purchase
    {
        public double Discount { get; set; }

        public PricePurchase(string name, double price, int quantity, double discount) : base(name, price, quantity)
        {
            Discount = discount;
        }

        public override double GetCost()
        {
            return ((Price - Discount) * Quantity);
        }

        public override string ToString()
        {
            string str;
            if (Discount == 0)
                str = String.Format("{0} {1,10}", base.ToString(), Discount.ToString(" "));
            else
                str = String.Format("{0} {1,10}", base.ToString(), Discount.ToString("0.00"));
            return (str);
        }
    }
}
