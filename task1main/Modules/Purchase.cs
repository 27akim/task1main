﻿using System;

namespace task1main.Modules
{
    class Purchase
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }

        public Purchase()
        {

        }

        public Purchase(string name, double price, int quantity)
        {
            Name = name;
            Price = price;
            Quantity = quantity;
        }

        public virtual double GetCost()
        {
            return (Price * Quantity);
        }

        public override string ToString()
        {
            string str = String.Format("{0,10} {1,10} {2,10} {3,10}",
                    Name, Price.ToString("0.00"), Quantity, GetCost().ToString("0.00"));
            return (str);
        }

        public override bool Equals(Object product)
        {
            Purchase p = (Purchase)product;
            return ((Price == p.Price) && (Name == p.Name));
        }
    }
}

