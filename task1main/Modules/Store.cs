﻿using System;
using System.Collections.Generic;
using System.IO;
using task1main.Exeptions;

namespace task1main.Modules
{
    class Store
    {
        private const string FileFormat = ".csv";
        private const int PurchaseVariablesCount = 3;
        private const int PricePurchaseVariablesCount = 4;
        private List<Purchase> _list = new List<Purchase>();

        public Store()
        {

        }

        public Store(string path)
        {
            string filePath = path + FileFormat;
            StreamReader data = OpenFile(filePath);
            _list = ReadData(data);
        }

        private StreamReader OpenFile(string filePath)
        {
            try
            {
                return new StreamReader(filePath);
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("File not found!");
                return null;
            }
        }

        private List<Purchase> ReadData(StreamReader data)
        {
            List<Purchase> list = new List<Purchase>();
            string s;
            while ((s = data.ReadLine()) != null)
            {
                string[] purchases = s.Split(';');
                try
                {
                    if (purchases.Length < PurchaseVariablesCount)
                        throw new CsvLineException(s, "Not enough arguments!");
                    if (purchases.Length > PricePurchaseVariablesCount)
                        throw new CsvLineException(s, "Too many arguments!");
                    if (String.IsNullOrEmpty(purchases[0]))
                        throw new CsvLineException(s, "Name is empty!");
                    if (!double.TryParse(purchases[1], out double res1))
                        throw new CsvLineException(s, "Incorrect format!");
                    if (!int.TryParse(purchases[2], out int res2))
                        throw new CsvLineException(s, "Incorrect format!");
                    if (purchases.Length == PricePurchaseVariablesCount)
                    {
                        if (!double.TryParse(purchases[3], out double res3))
                            throw new CsvLineException(s, "Incorrect format!");
                        if (double.Parse(purchases[3]) > double.Parse(purchases[1]))
                            throw new CsvLineException(s, "Discount is more than price!");
                        list.Add(CreatePurchase(purchases));
                    }
                    else
                        list.Add(CreatePurchase(purchases));
                }
                catch (CsvLineException e)
                {
                    Console.WriteLine("Error line: " + e.Line + " - " + e.Message);
                }
            }
            return list;
        }

        private Purchase CreatePurchase(string[] purchases)
        {
            string name = null;
            double price = 0;
            int quantity = 0;
            double discount = 0;
            name = purchases[0].ToString();
            price = double.Parse(purchases[1]);
            quantity = int.Parse(purchases[2]);
            if (purchases.Length == PricePurchaseVariablesCount)
            {
                discount = double.Parse(purchases[3]);
                PricePurchase somePricePurchase = new PricePurchase(name, price, quantity, discount);
                return somePricePurchase;
            }
            else
            {
                Purchase somePurchase = new Purchase(name, price, quantity);
                return somePurchase;
            }
        }

        public void Insert(int index, Purchase p)
        {
            if (index > _list.Count || index < 0)
                _list.Add(p);
            else
                _list.Insert(index, p);
        }

        public int Delete(int index)
        {
            if (index >= _list.Count || index < 0)
                return -1;
            else
            {
                _list.RemoveAt(index);
                return index;
            }
        }

        public double TotalCost()
        {
            double totalCost = 0;
            foreach (Purchase somePurchase in _list)
            {
                totalCost = totalCost + somePurchase.GetCost();
            }
            return (totalCost);
        }

        public void Print()
        {
            string str = String.Format("{0,10} {1,10} {2,10} {3,10} {4,10}",
                "Name", "Price", "Number", "Cost", "Discount");
            Console.WriteLine(str);
            foreach (Purchase somePurchase in _list)
            {
                Console.WriteLine(somePurchase);
            }
            str = String.Format("{0,10} {1,10} {2,10} {3,10}", "Total cost", " ", " ", TotalCost());
            Console.WriteLine(str);
        }

        public List<Purchase> Collection()
        {
            return _list;
        }
    }
}

