﻿using System;
using task1main.Modules;
using task1main.Comparers;

namespace task1main
{
    class Program
    {
        static void Main(string[] args)
        {
            Store someStore = new Store(args[0]);
            someStore.Print();
            Store otherStore = new Store(args[1]);
            someStore.Insert(0, otherStore.Collection()[otherStore.Collection().Count - 1]);
            someStore.Insert(1000, otherStore.Collection()[0]);
            someStore.Insert(2, otherStore.Collection()[2]);
            someStore.Delete(3);
            someStore.Delete(10);
            someStore.Delete(-5);
            someStore.Print();
            PurchaseComparer comparer = new PurchaseComparer(); 
            someStore.Collection().Sort(comparer);
            someStore.Print();
            otherStore.Print();
            FindElement(someStore, otherStore, 1, comparer);
            FindElement(someStore, otherStore, 3, comparer);
            Console.ReadLine();
        }

        static void FindElement(Store findStore, Store store, int index, PurchaseComparer comparer)
        {
            int position;
            position = findStore.Collection().BinarySearch(store.Collection()[index], comparer);
            if (position < 0)
                Console.WriteLine("Element " + store.Collection()[index] + " not found");
            else
                Console.WriteLine("Element " + store.Collection()[index] + " found at position " + position);
        }
    }
}
